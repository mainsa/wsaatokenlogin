﻿using ClientWsaaLogin;
using ClientWsaaLogin.WsaaLogin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace MainToken
{

        /// <summary>
        /// Clase principal
        /// </summary>
        /// <remarks></remarks>
        public class ProgramaPrincipal
        {
            // Valores por defecto, globales en esta clase
            const string DEFAULT_URLWSAAWSDL = "https://securetest.aduana.gov.py/wsaaserver/Server";
            const string DEFAULT_SERVICIO = "servicio";
            const string DEFAULT_CERTSIGNER = "c:\\CertificadoConClavePrivada.p12";
            const string DEFAULT_PROXY = null;
            const string DEFAULT_PROXY_USER = null;
            const string DEFAULT_PROXY_PASSWORD = null;
            const bool DEFAULT_VERBOSE = true;

            /// <summary>
            /// Funcion Main (consola)
            /// </summary>
            /// <param name="args">Argumentos de linea de comandos</param>
            /// <returns>0 si terminó bien, valores negativos si hubieron errores</returns>
            /// <remarks></remarks>
            public static int Main(string[] args)
            {
                const string ID_FNC = "[Main]";

                string strUrlWsaaWsdl = DEFAULT_URLWSAAWSDL;
                string strIdServicioNegocio = DEFAULT_SERVICIO;
                string strRutaCertSigner = DEFAULT_CERTSIGNER;
                SecureString strPasswordSecureString = new SecureString() ;
                //strPasswordSecureString = "yt4ufe";
                bool blnVerboseMode = DEFAULT_VERBOSE;

                // Argumentos OK, entonces procesar normalmente...

                LoginTicket objTicketRespuesta = null;
                string strTicketRespuesta = null;

                try
                {
                    if (blnVerboseMode)
                    {
                        Console.WriteLine(ID_FNC + "***Servicio a acceder: {0}", strIdServicioNegocio);
                        Console.WriteLine(ID_FNC + "***URL del WSAA: {0}", strUrlWsaaWsdl);
                        Console.WriteLine(ID_FNC + "***Ruta del certificado: {0}", strRutaCertSigner);
                        Console.WriteLine(ID_FNC + "***Modo verbose: {0}", blnVerboseMode);
                    }
                //Add Pass

                foreach (char c in "abcde")
                {
                    strPasswordSecureString.AppendChar(c);
                }
                strPasswordSecureString.MakeReadOnly();
                objTicketRespuesta = new LoginTicket();
                if (blnVerboseMode) Console.WriteLine(ID_FNC + "***Accediendo a {0}", strUrlWsaaWsdl);
                strTicketRespuesta = objTicketRespuesta.ObtenerLoginTicketResponse();
                //LoginTicketDev dev = new LoginTicketDev();
                //dev.ObtenerLoginTicketResponse();
            }
                catch (Exception excepcionAlObtenerTicket)
                {
                    Console.WriteLine(ID_FNC + "***EXCEPCION AL OBTENER TICKET: " + excepcionAlObtenerTicket.Message);
                    return -10;
                }
                return 0;
            }
        }

}
