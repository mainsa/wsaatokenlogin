﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Xml;
using System.Net;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography.Pkcs;


namespace ClientWsaaLogin{
    public class LoginTicket
    {

        /// <summary>
        /// Construye un Login Ticket obtenido del WSAA, luego de instanciar un objeto LoginTicket llamar al metodo
        /// </summary>
        /// <param name="argServicio">Servicio al que se desea acceder</param>
        /// <param name="argUrlWsaa">URL del WSAA</param>
        /// <param name="argRutaCertX509Firmante">Ruta del certificado X509 (con clave privada) usado para firmar</param>
        /// <param name="argPassword">Password del certificado X509 (con clave privada) usado para firmar</param>
        /// <param name="argProxy">IP:port del proxy</param>
        /// <param name="argProxyUser">Usuario del proxy</param>''' 
        /// <param name="argProxyPassword">Password del proxy</param>
        /// <param name="argVerbose">Nivel detallado de descripcion? true/false</param>
        /// <remarks></remarks>
        public string ObtenerLoginTicketResponse()
        {
            //Parametros del Token
            const string Source = "C=PY, O=DNA, OU=SOFIA, CN=xxxx";
            const string Destination = "C=PY,O=DNA,OU=sofia,CN=wsaatest";
            UInt32 UniqueId = 0; // OJO! NO ES THREAD-SAFE // Entero de 32 bits sin signo que identifica el requerimiento
            DateTime GenerationTime; // Momento en que fue generado el requerimiento
            DateTime ExpirationTime; // Momento en el que expira la solicitud
            const string Service = "servicio"; // Identificacion del WSN para el cual se solicita el TA
            string Sign; // Firma de seguridad recibida en la respuesta
            string Token; // Token de seguridad recibido en la respuesta

            //Lectura del certificado
            bool _verboseMode = true;
            SecureString strPasswordSecureString = new SecureString();
            CertificadosX509Lib.VerboseMode = _verboseMode;
            const string RutaDelCertificadoFirmante = "c:\\dev.p12";


            string cmsFirmadoBase64 = null;
            string loginTicketResponse = null;
            string XmlStrLoginTicketRequestTemplate = "<loginTicketRequest version='1.0'><header><source></source><destination></destination><uniqueId></uniqueId><generationTime></generationTime><expirationTime></expirationTime></header><service></service></loginTicketRequest>";
            XmlDocument XmlLoginTicketRequest = null;
            XmlDocument XmlLoginTicketResponse = null;
            XmlNode xmlNodoSource = default(XmlNode);
            XmlNode xmlNodoDestination = default(XmlNode);
            XmlNode xmlNodoUniqueId = default(XmlNode);
            XmlNode xmlNodoGenerationTime = default(XmlNode);
            XmlNode xmlNodoExpirationTime = default(XmlNode);
            XmlNode xmlNodoService = default(XmlNode);

            foreach (char c in "1234567")
            {
                strPasswordSecureString.AppendChar(c);
            }
            strPasswordSecureString.MakeReadOnly();

            // PASO 1: Genero el Login Ticket Request
            try
            {
                UniqueId += 1;
                XmlLoginTicketRequest = new XmlDocument();
                XmlLoginTicketRequest.LoadXml(XmlStrLoginTicketRequestTemplate);
                xmlNodoSource = XmlLoginTicketRequest.SelectSingleNode("//source");
                xmlNodoSource.InnerText = Source;
                xmlNodoDestination = XmlLoginTicketRequest.SelectSingleNode("//destination");
                xmlNodoDestination.InnerText = Destination;
                xmlNodoUniqueId = XmlLoginTicketRequest.SelectSingleNode("//uniqueId");
                xmlNodoUniqueId.InnerText = Convert.ToString(UniqueId);
                xmlNodoGenerationTime = XmlLoginTicketRequest.SelectSingleNode("//generationTime");
                xmlNodoGenerationTime.InnerText = DateTime.Now.ToString("s");
                xmlNodoExpirationTime = XmlLoginTicketRequest.SelectSingleNode("//expirationTime");
                //xmlNodoExpirationTime.InnerText = DateTime.Now.AddHours(24).ToString("s");
                xmlNodoExpirationTime.InnerText = DateTime.Now.AddMinutes(10).ToString("s");
                xmlNodoService = XmlLoginTicketRequest.SelectSingleNode("//service");
                xmlNodoService.InnerText = Service;
            }
            catch (Exception excepcionAlGenerarLoginTicketRequest)
            {
                throw new Exception("***Error GENERANDO el LoginTicketRequest : " + excepcionAlGenerarLoginTicketRequest.Message + excepcionAlGenerarLoginTicketRequest.StackTrace);
            }

            // PASO 2: Firmo el Login Ticket Request
            try
            {
                X509Certificate2 certFirmante = CertificadosX509Lib.ObtieneCertificadoDesdeArchivo(RutaDelCertificadoFirmante, strPasswordSecureString);

                // Convierto el Login Ticket Request a bytes, firmo el msg y lo convierto a Base64
                Encoding EncodedMsg = Encoding.UTF8;
                byte[] msgBytes = EncodedMsg.GetBytes(XmlLoginTicketRequest.OuterXml);
                byte[] encodedSignedCms = CertificadosX509Lib.FirmaBytesMensaje(msgBytes, certFirmante);
                cmsFirmadoBase64 = Convert.ToBase64String(encodedSignedCms);
            }
            catch (Exception excepcionAlFirmar)
            {
                throw new Exception("***Error FIRMANDO el LoginTicketRequest : " + excepcionAlFirmar.Message);
            }

            // PASO 3: Invoco al WSAA para obtener el Login Ticket Response
            try
            {
                //WsaaLoginDev.WsaaServerBeanClient loginService = new WsaaLoginDev.WsaaServerBeanClient();
                //loginTicketResponse = loginService.loginCms(cmsFirmadoBase64);
            }
            catch (Exception excepcionAlInvocarWsaa)
            {
                throw new Exception("***Error INVOCANDO al servicio WSAA : " + excepcionAlInvocarWsaa.Message);
            }

            // PASO 4: Analizo el Login Ticket Response recibido del WSAA
            try
            {
                XmlLoginTicketResponse = new XmlDocument();
                XmlLoginTicketResponse.LoadXml(loginTicketResponse);

                UniqueId = UInt32.Parse(XmlLoginTicketResponse.SelectSingleNode("//uniqueId").InnerText);
                GenerationTime = DateTime.Parse(XmlLoginTicketResponse.SelectSingleNode("//generationTime").InnerText);
                ExpirationTime = DateTime.Parse(XmlLoginTicketResponse.SelectSingleNode("//expirationTime").InnerText);
                Sign = XmlLoginTicketResponse.SelectSingleNode("//sign").InnerText;
                Token = XmlLoginTicketResponse.SelectSingleNode("//token").InnerText;
            }
            catch (Exception excepcionAlAnalizarLoginTicketResponse)
            {
                throw new Exception("***Error ANALIZANDO el LoginTicketResponse : " + excepcionAlAnalizarLoginTicketResponse.Message);
            }
            return loginTicketResponse;
        }
    }

/// <summary>
/// Libreria de utilidades para manejo de certificados
/// </summary>
/// <remarks></remarks>
    public class CertificadosX509Lib
    {
        public static bool VerboseMode = false;

        /// <summary>
        /// Firma mensaje
        /// </summary>
        /// <param name="argBytesMsg">Bytes del mensaje</param>
        /// <param name="argCertFirmante">Certificado usado para firmar</param>
        /// <returns>Bytes del mensaje firmado</returns>
        /// <remarks></remarks>
        public static byte[] FirmaBytesMensaje(byte[] argBytesMsg, X509Certificate2 argCertFirmante)
        {
            const string ID_FNC = "[FirmaBytesMensaje]";
            try
            {
                // Pongo el mensaje en un objeto ContentInfo (requerido para construir el obj SignedCms)
                ContentInfo infoContenido = new ContentInfo(argBytesMsg);
                SignedCms cmsFirmado = new SignedCms(infoContenido);

                // Creo objeto CmsSigner que tiene las caracteristicas del firmante
                CmsSigner cmsFirmante = new CmsSigner(argCertFirmante);
                cmsFirmante.IncludeOption = X509IncludeOption.EndCertOnly;

                if (VerboseMode) Console.WriteLine(ID_FNC + "***Firmando bytes del mensaje...");

                // Firmo el mensaje PKCS #7
                cmsFirmado.ComputeSignature(cmsFirmante);

                if (VerboseMode) Console.WriteLine(ID_FNC + "***OK mensaje firmado");

                // Encodeo el mensaje PKCS #7.
                return cmsFirmado.Encode();
            }
            catch (Exception excepcionAlFirmar)
            {
                throw new Exception(ID_FNC + "***Error al firmar: " + excepcionAlFirmar.Message);
            }
        }

        /// <summary>
        /// Lee certificado de disco
        /// </summary>
        /// <param name="argArchivo">Ruta del certificado a leer.</param>
        /// <returns>Un objeto certificado X509</returns>
        /// <remarks></remarks>
        public static X509Certificate2 ObtieneCertificadoDesdeArchivo(string argArchivo, SecureString argPassword)
        {
            const string ID_FNC = "[ObtieneCertificadoDesdeArchivo]";
            X509Certificate2 objCert = new X509Certificate2();
            try
            {
                if (argPassword.IsReadOnly())
                {
                    objCert.Import(File.ReadAllBytes(argArchivo), argPassword, X509KeyStorageFlags.PersistKeySet);
                }
                else
                {
                    objCert.Import(File.ReadAllBytes(argArchivo));
                }
                return objCert;
            }
            catch (Exception excepcionAlImportarCertificado)
            {
                throw new Exception(ID_FNC + "***Error al leer certificado: " + excepcionAlImportarCertificado.Message);
            }
        }
    }

}
